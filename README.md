# Raspberry pi QRcode Reader

before I was wrote this code, I using another code.
but... That's not working...
So, I write code that use two bash command.

## Depended
1. PI Camera Enable
	```
	sudo raspi-config
    ```
	![you](https://dab1nmslvvntp.cloudfront.net/wp-content/uploads/2015/07/1436675616rpicameraenable.png)
2. zbar-tools
	If you not install this tool, you should install [zbar-tools](http://packages.ubuntu.com/precise/graphics/zbar-tools).
    ```
    sudo apt-get install zbar-tools
    ```

### How to use?
1. Connect webcam to Raspberry Pi.
2. Run **qr.py**:

    ```
    python qr.py
    ```

3. WORK
    ![Raspberry Pi Node.js Barcode Reader](https://cdn.discordapp.com/attachments/268533025443282955/287672806508855308/unknown.png)