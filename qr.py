import subprocess, threading
import picamera

class Command(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None

    def run(self, timeout):
        def target():
            print 'Thread started'
            self.process = subprocess.Popen(self.cmd, shell=True, stdout=subprocess.PIPE)
            self.out, self.err = self.process.communicate()
            # print("OUTPUT : " + self.out[8:])
            print 'Thread finished'

        thread = threading.Thread(target=target)
        thread.start()

        thread.join(timeout)
        if thread.is_alive():
            print 'Terminating process'
            self.process.terminate()
            thread.join()
        if int(self.process.returncode) == 0 : 
            print self.out[8:]

while True :
	with picamera.PiCamera() as camera :
		camera.capture("image.png")
		camera.close()	
	command = Command("zbarimg image.png")
	command.run(timeout=1)
